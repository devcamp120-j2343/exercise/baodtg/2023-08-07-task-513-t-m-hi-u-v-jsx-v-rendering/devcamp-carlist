import { checkDateOfCar, gCars } from "./info";
function App() {
  return (
    <div className="App">
    
      <ul>
        {gCars.map((val, index) => {
          return <li key={index}>
            {val.make} - {val.vID} - {checkDateOfCar(val.year)}
          </li>
        })}


      </ul>
    </div>
  );
}

export default App;
